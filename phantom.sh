#!/bin/sh 

host="$(hostname)"
os="$(lsb_release -ds)"
kernel="$(uname -sr)"
uptime="$(uptime -p | sed 's/up //')"
packages="$(dpkg -l | wc -l)"
shell="$(basename "${SHELL}")"

printf "              \
        $(tput sgr0)$(tput bold)$(tput setaf 4)${USER}$(tput sgr0)@$(tput bold)$(tput setaf 4)$(hostname)\n"
printf "              \
        $(tput sgr0)$(tput bold)$(tput setaf 4)OS:        $(tput sgr0)${os}\n"
printf "\
\e[38;2;67;81;138m▇▇\e[38;2;80;97;164m▇▇\e[38;2;80;97;164m▇▇\e[38;2;80;97;164m▇▇\e[38;2;80;97;164m▇▇\e[38;2;80;97;164m▇▇\e[38;2;67;81;138m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 4)KERNEL:    $(tput sgr0)${kernel}\n"
printf "\
\e[38;2;73;217;2m▇▇\e[38;2;32;200;8m▇▇\e[38;2;80;97;164m▇▇\e[38;2;80;97;164m▇▇\e[38;2;80;97;164m▇▇\e[38;2;32;200;8m▇▇\e[38;2;73;217;2m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 4)UPTIME:    $(tput sgr0)${uptime}\n"
printf "\
\e[38;2;80;97;164m▇▇\e[38;2;67;81;138m▇▇\e[38;2;80;97;164m▇▇\e[38;2;80;97;164m▇▇\e[38;2;80;97;164m▇▇\e[38;2;67;81;138m▇▇\e[38;2;80;97;164m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 4)PACKAGES:  $(tput sgr0)${packages}\n"
printf "              \
        $(tput sgr0)$(tput bold)$(tput setaf 4)SHELL:     $(tput sgr0)${shell}\n"
printf "              \
        $(tput sgr0)$(tput bold)$(tput setaf 4)DATE:      $(tput sgr0)$(date)\n"
printf "
"