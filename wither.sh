#!/bin/sh 

host="$(hostname)"
os="$(lsb_release -ds)"
kernel="$(uname -sr)"
uptime="$(uptime -p | sed 's/up //')"
packages="$(dpkg -l | wc -l)"
shell="$(basename "${SHELL}")"

printf "\
\e[38;2;43;43;43m▇▇\e[38;2;37;37;37m▇▇\e[38;2;44;44;44m▇▇\e[38;2;28;28;28m▇▇\e[38;2;20;20;20m▇▇\e[38;2;26;26;26m▇▇\e[38;2;37;37;37m▇▇\e[38;2;34;34;34m▇▇\n"
printf "\
\e[38;2;37;37;37m▇▇\e[38;2;37;37;37m▇▇\e[38;2;37;37;37m▇▇\e[38;2;51;51;51m▇▇\e[38;2;71;71;71m▇▇\e[38;2;69;69;69m▇▇\e[38;2;37;37;37m▇▇\e[38;2;28;28;28m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)${USER}$(tput sgr0)@$(tput bold)$(tput setaf 0)$(hostname)\n"
printf "\
\e[38;2;37;37;37m▇▇\e[38;2;77;77;77m▇▇\e[38;2;80;80;80m▇▇\e[38;2;92;92;92m▇▇\e[38;2;84;84;84m▇▇\e[38;2;84;84;84m▇▇\e[38;2;65;65;65m▇▇\e[38;2;51;51;51m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)OS:        $(tput sgr0)${os}\n"
printf "\
\e[38;2;20;20;20m▇▇\e[38;2;20;20;20m▇▇\e[38;2;20;20;20m▇▇\e[38;2;71;71;71m▇▇\e[38;2;55;55;55m▇▇\e[38;2;20;20;20m▇▇\e[38;2;20;20;20m▇▇\e[38;2;20;20;20m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)KERNEL:    $(tput sgr0)${kernel}\n"
printf "\
\e[38;2;74;74;74m▇▇\e[38;2;193;193;193m▇▇\e[38;2;193;193;193m▇▇\e[38;2;74;74;74m▇▇\e[38;2;80;80;80m▇▇\e[38;2;193;193;193m▇▇\e[38;2;193;193;193m▇▇\e[38;2;65;65;65m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)UPTIME:    $(tput sgr0)${uptime}\n"
printf "\
\e[38;2;48;48;48m▇▇\e[38;2;71;71;71m▇▇\e[38;2;71;71;71m▇▇\e[38;2;80;80;80m▇▇\e[38;2;71;71;71m▇▇\e[38;2;80;80;80m▇▇\e[38;2;51;51;51m▇▇\e[38;2;26;26;26m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)PACKAGES:  $(tput sgr0)${packages}\n"
printf "\
\e[38;2;40;40;40m▇▇\e[38;2;20;20;20m▇▇\e[38;2;193;193;193m▇▇\e[38;2;193;193;193m▇▇\e[38;2;193;193;193m▇▇\e[38;2;193;193;193m▇▇\e[38;2;20;20;20m▇▇\e[38;2;28;28;28m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)SHELL:     $(tput sgr0)${shell}\n"
printf "\
\e[38;2;20;20;20m▇▇\e[38;2;12;12;12m▇▇\e[38;2;20;20;20m▇▇\e[38;2;20;20;20m▇▇\e[38;2;31;31;31m▇▇\e[38;2;20;20;20m▇▇\e[38;2;31;31;31m▇▇\e[38;2;20;20;20m▇▇\n"
printf "
"