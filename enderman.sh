#!/bin/sh 

host="$(hostname)"
os="$(lsb_release -ds)"
kernel="$(uname -sr)"
uptime="$(uptime -p | sed 's/up //')"
packages="$(dpkg -l | wc -l)"
shell="$(basename "${SHELL}")"

printf "\
\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\n"
printf "\
\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)${USER}$(tput sgr0)@$(tput bold)$(tput setaf 0)$(hostname)\n"
printf "\
\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)OS:        $(tput sgr0)${os}\n"
printf "\
\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)KERNEL:    $(tput sgr0)${kernel}\n"
printf "\
\e[38;2;224;121;250m▇▇\e[38;2;204;0;250m▇▇\e[38;2;224;121;250m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;224;121;250m▇▇\e[38;2;204;0;250m▇▇\e[38;2;224;121;250m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)UPTIME:    $(tput sgr0)${uptime}\n"
printf "\
\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)PACKAGES:  $(tput sgr0)${packages}\n"
printf "\
\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)SHELL:     $(tput sgr0)${shell}\n"
printf "\
\e[38;2;22;22;22m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;22;22;22m▇▇\n"
printf "
"