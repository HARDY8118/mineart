#!/bin/sh 

host="$(hostname)"
os="$(lsb_release -ds)"
kernel="$(uname -sr)"
uptime="$(uptime -p | sed 's/up //')"
packages="$(dpkg -l | wc -l)"
shell="$(basename "${SHELL}")"

printf "\
\e[38;2;43;43;43m▇▇\e[38;2;37;37;37m▇▇\e[38;2;44;44;44m▇▇\e[38;2;28;28;28m▇▇\e[38;2;20;20;20m▇▇\e[38;2;26;26;26m▇▇\e[38;2;37;37;37m▇▇\e[38;2;34;34;34m▇▇\n"
printf "\
\e[38;2;67;71;70m▇▇\e[38;2;52;54;53m▇▇\e[38;2;52;54;53m▇▇\e[38;2;26;26;26m▇▇\e[38;2;44;44;44m▇▇\e[38;2;64;66;65m▇▇\e[38;2;52;54;53m▇▇\e[38;2;64;68;67m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)${USER}$(tput sgr0)@$(tput bold)$(tput setaf 0)$(hostname)\n"
printf "\
\e[38;2;37;37;37m▇▇\e[38;2;50;50;50m▇▇\e[38;2;41;41;41m▇▇\e[38;2;50;50;50m▇▇\e[38;2;47;47;47m▇▇\e[38;2;41;41;41m▇▇\e[38;2;50;50;50m▇▇\e[38;2;52;52;52m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)OS:        $(tput sgr0)${os}\n"
printf "\
\e[38;2;76;80;79m▇▇\e[38;2;49;55;53m▇▇\e[38;2;55;55;55m▇▇\e[38;2;52;52;52m▇▇\e[38;2;60;66;64m▇▇\e[38;2;67;71;70m▇▇\e[38;2;71;75;74m▇▇\e[38;2;75;77;76m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)KERNEL:    $(tput sgr0)${kernel}\n"
printf "\
\e[38;2;74;74;74m▇▇\e[38;2;7;7;7m▇▇\e[38;2;7;7;7m▇▇\e[38;2;52;52;52m▇▇\e[38;2;52;52;52m▇▇\e[38;2;7;7;7m▇▇\e[38;2;7;7;7m▇▇\e[38;2;65;65;65m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)UPTIME:    $(tput sgr0)${uptime}\n"
printf "\
\e[38;2;89;93;92m▇▇\e[38;2;66;68;67m▇▇\e[38;2;52;52;52m▇▇\e[38;2;41;41;41m▇▇\e[38;2;52;52;52m▇▇\e[38;2;52;52;52m▇▇\e[38;2;59;59;59m▇▇\e[38;2;78;80;79m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)PACKAGES:  $(tput sgr0)${packages}\n"
printf "\
\e[38;2;40;40;40m▇▇\e[38;2;44;44;44m▇▇\e[38;2;7;7;7m▇▇\e[38;2;7;7;7m▇▇\e[38;2;7;7;7m▇▇\e[38;2;7;7;7m▇▇\e[38;2;41;41;41m▇▇\e[38;2;28;28;28m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 0)SHELL:     $(tput sgr0)${shell}\n"
printf "\
\e[38;2;58;62;61m▇▇\e[38;2;48;50;49m▇▇\e[38;2;55;57;56m▇▇\e[38;2;48;50;49m▇▇\e[38;2;48;50;49m▇▇\e[38;2;55;57;56m▇▇\e[38;2;62;66;65m▇▇\e[38;2;73;84;80m▇▇\n"
printf "
"