#!/bin/sh 

host="$(hostname)"
os="$(lsb_release -ds)"
kernel="$(uname -sr)"
uptime="$(uptime -p | sed 's/up //')"
packages="$(dpkg -l | wc -l)"
shell="$(basename "${SHELL}")"

printf "\
\e[38;2;22;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;25;0;0m▇▇\e[38;2;52;4;0m▇▇\e[38;2;52;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;4;0;0m▇▇\n"
printf "\
\e[38;2;10;0;0m▇▇\e[38;2;10;0;0m▇▇\e[38;2;29;0;0m▇▇\e[38;2;48;0;0m▇▇\e[38;2;52;26;0m▇▇\e[38;2;52;2;0m▇▇\e[38;2;25;0;0m▇▇\e[38;2;14;0;0m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 1)${USER}$(tput sgr1)@$(tput bold)$(tput setaf 1)$(hostname)\n"
printf "\
\e[38;2;18;0;0m▇▇\e[38;2;205;0;0m▇▇\e[38;2;205;0;0m▇▇\e[38;2;14;0;0m▇▇\e[38;2;14;0;0m▇▇\e[38;2;205;0;0m▇▇\e[38;2;205;0;0m▇▇\e[38;2;18;0;0m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 1)OS:        $(tput sgr0)${os}\n"
printf "\
\e[38;2;38;0;0m▇▇\e[38;2;252;87;0m▇▇\e[38;2;252;252;0m▇▇\e[38;2;48;0;0m▇▇\e[38;2;10;0;0m▇▇\e[38;2;252;252;0m▇▇\e[38;2;252;87;0m▇▇\e[38;2;42;0;0m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 1)KERNEL:    $(tput sgr0)${kernel}\n"
printf "\
\e[38;2;18;0;0m▇▇\e[38;2;52;2;0m▇▇\e[38;2;52;26;0m▇▇\e[38;2;14;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;35;0;0m▇▇\e[38;2;52;0;0m▇▇\e[38;2;52;0;0m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 1)UPTIME:    $(tput sgr0)${uptime}\n"
printf "\
\e[38;2;14;0;0m▇▇\e[38;2;14;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;14;0;0m▇▇\e[38;2;48;0;0m▇▇\e[38;2;52;0;0m▇▇\e[38;2;52;8;0m▇▇\e[38;2;52;26;0m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 1)PACKAGES:  $(tput sgr0)${packages}\n"
printf "\
\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;10;0;0m▇▇\e[38;2;38;0;0m▇▇\e[38;2;52;26;0m▇▇\e[38;2;52;0;0m▇▇\e[38;2;52;0;0m▇▇\e[38;2;52;0;0m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 1)SHELL:     $(tput sgr0)${shell}\n"
printf "\
\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;52;8;0m▇▇\e[38;2;52;26;0m▇▇\e[38;2;36;0;0m▇▇\e[38;2;10;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;48;0;0m▇▇\n"
printf "
"