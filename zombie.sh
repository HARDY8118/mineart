#!/bin/sh 

host="$(hostname)"
os="$(lsb_release -ds)"
kernel="$(uname -sr)"
uptime="$(uptime -p | sed 's/up //')"
packages="$(dpkg -l | wc -l)"
shell="$(basename "${SHELL}")"

printf "\
\e[38;2;0;98;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;98;20m▇▇\e[38;2;0;98;20m▇▇\e[38;2;0;69;20m▇▇\e[38;2;0;69;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;88;20m▇▇\n"
printf "\
\e[38;2;0;88;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;98;20m▇▇\e[38;2;0;118;20m▇▇\e[38;2;0;118;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;88;20m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 2)${USER}$(tput sgr0)@$(tput bold)$(tput setaf 2)$(hostname)\n"
printf "\
\e[38;2;0;88;20m▇▇\e[38;2;0;135;63m▇▇\e[38;2;0;157;94m▇▇\e[38;2;0;135;63m▇▇\e[38;2;0;157;94m▇▇\e[38;2;0;157;94m▇▇\e[38;2;0;167;94m▇▇\e[38;2;0;118;20m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 2)OS:        $(tput sgr0)${os}\n"
printf "\
\e[38;2;0;135;63m▇▇\e[38;2;0;135;63m▇▇\e[38;2;0;167;94m▇▇\e[38;2;0;118;20m▇▇\e[38;2;0;135;63m▇▇\e[38;2;0;167;94m▇▇\e[38;2;0;157;94m▇▇\e[38;2;0;118;20m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 2)KERNEL:    $(tput sgr0)${kernel}\n"
printf "\
\e[38;2;0;118;20m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;157;94m▇▇\e[38;2;0;135;63m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;0;0m▇▇\e[38;2;0;135;63m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 2)UPTIME:    $(tput sgr0)${uptime}\n"
printf "\
\e[38;2;0;98;20m▇▇\e[38;2;0;135;63m▇▇\e[38;2;0;135;63m▇▇\e[38;2;0;59;0m▇▇\e[38;2;0;59;0m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;69;20m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 2)PACKAGES:  $(tput sgr0)${packages}\n"
printf "\
\e[38;2;0;98;20m▇▇\e[38;2;0;98;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;98;20m▇▇\e[38;2;0;98;20m▇▇\e[38;2;0;69;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;69;20m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 2)SHELL:     $(tput sgr0)${shell}\n"
printf "\
\e[38;2;0;69;20m▇▇\e[38;2;0;69;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;98;20m▇▇\e[38;2;0;69;20m▇▇\e[38;2;0;88;20m▇▇\e[38;2;0;69;20m▇▇\n"
printf "
"