# Mineart
Set of shell scripts to output minecraft mobs and items on console

## Running
Change the file to executable using 

```bash
chmod +x [scriptname]
```

Run the file 

```bash
./[script]
```

## Random output on shell startup
```bash
if [ -e $HOME/Mineart ]; then
     f=$( find $HOME/Mineart/*.sh -type f | shuf -n 1 )
     $f
fi

```

To run everytime on startup, clone the repo then add following in `~/.bashrc`

```bash
if [ -f $HOME/randomizer_script ]; then
	./randomizer_script
fi
```

## Machine information
Common information like kernel or os information is also displayed on side.

The functionality of getting and displaying machine information is directly taken from [ufetch](https://gitlab.com/jschx/ufetch).
