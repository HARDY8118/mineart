#!/bin/sh 

host="$(hostname)"
os="$(lsb_release -ds)"
kernel="$(uname -sr)"
uptime="$(uptime -p | sed 's/up //')"
packages="$(dpkg -l | wc -l)"
shell="$(basename "${SHELL}")"

printf "\
\e[38;2;50;148;0m▇▇\e[38;2;41;130;0m▇▇\e[38;2;50;148;0m▇▇\e[38;2;76;166;130m▇▇\e[38;2;77;146;128m▇▇\e[38;2;77;146;128m▇▇\e[38;2;80;110;106m▇▇\e[38;2;41;130;0m▇▇\n"
printf "\
\e[38;2;50;148;0m▇▇\e[38;2;80;110;106m▇▇\e[38;2;34;112;0m▇▇\e[38;2;86;132;126m▇▇\e[38;2;76;166;130m▇▇\e[38;2;86;132;126m▇▇\e[38;2;84;185;114m▇▇\e[38;2;86;132;126m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 6)${USER}$(tput sgr0)@$(tput bold)$(tput setaf 6)$(hostname)\n"
printf "\
\e[38;2;34;112;0m▇▇\e[38;2;86;132;126m▇▇\e[38;2;80;110;106m▇▇\e[38;2;77;146;128m▇▇\e[38;2;86;132;126m▇▇\e[38;2;77;146;128m▇▇\e[38;2;39;81;75m▇▇\e[38;2;80;110;106m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 6)OS:        $(tput sgr0)${os}\n"
printf "\
\e[38;2;80;110;106m▇▇\e[38;2;77;146;128m▇▇\e[38;2;86;132;126m▇▇\e[38;2;86;132;126m▇▇\e[38;2;77;146;128m▇▇\e[38;2;86;132;126m▇▇\e[38;2;86;132;126m▇▇\e[38;2;77;146;128m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 6)KERNEL:    $(tput sgr0)${kernel}\n"
printf "\
\e[38;2;86;132;126m▇▇\e[38;2;143;241;215m▇▇\e[38;2;143;241;215m▇▇\e[38;2;86;132;126m▇▇\e[38;2;86;132;126m▇▇\e[38;2;143;241;215m▇▇\e[38;2;143;241;215m▇▇\e[38;2;86;132;126m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 6)UPTIME:    $(tput sgr0)${uptime}\n"
printf "\
\e[38;2;86;132;126m▇▇\e[38;2;77;146;128m▇▇\e[38;2;73;114;108m▇▇\e[38;2;101;224;221m▇▇\e[38;2;101;224;221m▇▇\e[38;2;73;114;108m▇▇\e[38;2;73;114;108m▇▇\e[38;2;86;132;126m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 6)PACKAGES:  $(tput sgr0)${packages}\n"
printf "\
\e[38;2;86;132;126m▇▇\e[38;2;86;132;126m▇▇\e[38;2;143;241;215m▇▇\e[38;2;143;241;215m▇▇\e[38;2;143;241;215m▇▇\e[38;2;143;241;215m▇▇\e[38;2;86;132;126m▇▇\e[38;2;77;146;128m▇▇\
        $(tput sgr0)$(tput bold)$(tput setaf 6)SHELL:     $(tput sgr0)${shell}\n"
printf "\
\e[38;2;73;114;108m▇▇\e[38;2;86;132;126m▇▇\e[38;2;73;114;108m▇▇\e[38;2;73;114;108m▇▇\e[38;2;73;114;108m▇▇\e[38;2;73;114;108m▇▇\e[38;2;86;132;126m▇▇\e[38;2;80;110;106m▇▇\n"
printf "
"